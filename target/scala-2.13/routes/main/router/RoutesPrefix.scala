// @GENERATOR:play-routes-compiler
// @SOURCE:/home/a0131724/IdeaProjects/play-samples-play-scala-hello-world-tutorial/conf/routes
// @DATE:Sun Jul 21 20:52:35 JST 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
