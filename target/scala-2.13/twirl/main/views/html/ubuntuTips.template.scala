
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object ubuntuTips extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template0[play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply():play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*1.2*/main("UbuntuTips")/*1.20*/{_display_(Seq[Any](format.raw/*1.21*/("""
"""),format.raw/*2.1*/("""<section id="content">
    <div class="wrapper doc">
        <article>
            <h1>Ubuntu18.04で使えるコマンド集</h1>


            <h2>デスクトップ操作</h2>
            <table>
                <tr>
                    <th>コマンド</th><th>意味</th>
                </tr>
                <tr><td>Super</td><td>アクティビティ画面を表示(※Super=Windowsキー)</td></tr>
                <tr><td>Super + q</td><td>Dockのアイコン番号を一定時間表示</td></tr>
                <tr><td>Super + [0~9]</td><td>Dockにあるアプリケーションを起動or開いているアプリを最前面に表示</td></tr>
                <tr><td>Super + Shift + [0~9]</td><td>開いているアプリを新しいウィンドウで表示</td></tr>
                <tr><td>Super + a</td><td>アプリケーションを検索</td></tr>
                <tr><td>Super + m/v</td><td>カレンダーを表示</td></tr>
                <tr><td>Super + l</td><td>画面ロック</td></tr>
                <tr><td>Super + Home</td><td>最初のワークスペースに移動</td></tr>
                <tr><td>Super + End</td><td>最後のワークスペースに移動</td></tr>
                <tr><td>Ctrl + Alt + ↑/↓</td><td>上/下のワークスペースに移動</td></tr>
                <tr><td>Alt + F2</td><td>コマンドの実行</td></tr>
                <tr><td>Ctrl + Alt + BackSpace</td><td>GUIを強制終了してログイン画面に戻る(初期設定では無効)</td></tr>
                <tr><td>Ctrl + Alt + Delete</td><td>ログアウトダイアログを表示</td></tr>
                <tr><td>Ctrl + Alt + [F3~F6]</td><td>仮想コンソールに切り替える</td></tr>
                <tr><td>Ctrl + Alt + F2</td><td>GUIに戻る</td></tr>
            </table>

            <h2>ウィンドウ操作</h2>
            <table>
                <tr>
                    <th>コマンド</th><th>意味</th>
                </tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
            </table>

            <h2>キャプチャ</h2>
            <table>
                <tr>
                    <th>コマンド</th><th>意味</th>
                </tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
            </table>

            <h2>言語切替</h2>
            <table>
                <tr>
                    <th>コマンド</th><th>意味</th>
                </tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
                <tr><td></td><td></td></tr>
            </table>

            <h2>ファイルマネージャーの操作</h2>
            <table>
                <tr>
                    <th>コマンド</th><th>意味</th>
                </tr>
                <tr><td>Ctrl + n</td><td>新規ウィンドウを開く</td></tr>
                <tr><td>Ctrl + t</td><td>新規タブを開く</td></tr>
                <tr><td>Ctrl + PgUp/PgDn</td><td>タブを切り替える</td></tr>
                <tr><td>Ctrl + [1~9]</td><td>タブを切り替える(左から昇順)</td></tr>
                <tr><td>Ctrl + Shift + PgUp/PgDn</td><td>カレントタブを左/右に移動</td></tr>
                <tr><td>Ctrl + w</td><td>タブまたはウィンドウを閉じる</td></tr>
                <tr><td>Ctrl + Shift + t</td><td>閉じたタブをカレントタブの右側に復元</td></tr>
                <tr><td>Ctrl + d</td><td>現在の場所をブックマーク</td></tr>
                <tr><td>Enter</td><td rowspan="3">選択ファイルを開くor選択フォルダに移動</td></tr>
                <tr><td>Ctrl + o</td></td></tr>
                <tr><td>Alt + ↓</td></td></tr>
                <tr><td>Ctrl + Enter</td><td>選択ディレクトリを新規タブで開く</td></tr>
                <tr><td>Shift + Enter</td><td>選択ディレクトリを新規ウィンドウで開く</td></tr>
                <tr><td>Alt + ↑</td><td>親ディレクトリに移動</td></tr>
                <tr><td>Alt + ←</td><td>履歴を戻る</td></tr>
                <tr><td>Alt + →</td><td>履歴を進む</td></tr>
                <tr><td>Alt + Home</td><td>ホームディレクトリに移動</td></tr>
                <tr><td>Ctrl + l</td><td>ロケーションバーを表示</td></tr>
                <tr><td>Ctrl + +/-</td><td>拡大/縮小</td></tr>
                <tr><td>Ctrl + 0</td><td>拡縮サイズをデフォルト(100％)にする</td></tr>
                <tr><td>F5</td><td rowspan="2">更新</td></tr>
                <tr><td>Ctrl + r</td><td></td></tr>
                <tr><td>Ctrl + h</td><td>隠しファイルを表示/ブラウザの場合履歴表示</td></tr>
                <tr><td>F9</td><td>サイドバーを表示</td></tr>
                <tr><td>F10</td><td>ハンバーガーメニューを表示</td></tr>
                <tr><td>Ctrl + 1</td><td>リスト表示に切り替える</td></tr>
                <tr><td>Ctrl + 2</td><td>アイコン表示に切り替える</td></tr>
                <tr><td>Ctrl + Shift + n</td><td>新規ディレクトリを作成</td></tr>
                <tr><td>F1</td><td>ヘルプ</td></tr>
                <tr><td>Ctrl + F1</td><td>ショートカットキーの一覧を表示</td></tr>
                <tr><td>F2</td><td>選択ディレクトリ・ファイルの名前を変更</td></tr>
                <tr><td>Ctrl + x</td><td>選択ディレクトリ・ファイルの切り取り</td></tr>
                <tr><td>Ctrl + s</td><td>パターンを指定して選択</td></tr>
                <tr><td>Ctrl + Shift + i</td><td>選択を反転</td></tr>
                <tr><td>Ctrl + Shift + z</td><td>操作をやり直す(redo)</td></tr>
                <tr><td>Ctrl + i</td><td>選択ディレクトリ・ファイルのプロパティを表示</td></tr>


            </table>

        </article>

        <aside>
            """),_display_(/*117.14*/commonSidebar()),format.raw/*117.29*/("""
        """),format.raw/*118.9*/("""</aside>
    </div>
</section>
""")))}))
      }
    }
  }

  def render(): play.twirl.api.HtmlFormat.Appendable = apply()

  def f:(() => play.twirl.api.HtmlFormat.Appendable) = () => apply()

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2019-07-21T21:49:09.636571
                  SOURCE: /home/a0131724/IdeaProjects/play-samples-play-scala-hello-world-tutorial/app/views/ubuntuTips.scala.html
                  HASH: 2b80b475e3634e841eb5a581895b10f63c47c15d
                  MATRIX: 816->1|842->19|880->20|907->21|6115->5201|6152->5216|6189->5225
                  LINES: 26->1|26->1|26->1|27->2|142->117|142->117|143->118
                  -- GENERATED --
              */
          