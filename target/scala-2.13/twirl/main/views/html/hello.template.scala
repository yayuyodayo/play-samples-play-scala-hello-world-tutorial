
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object hello extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(name: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](_display_(/*2.2*/main("Hello")/*2.15*/ {_display_(Seq[Any](format.raw/*2.17*/("""
    """),format.raw/*3.5*/("""<section id="content">
        <div class="wrapper doc">
            <article>
                <h1>Hello World</h1>
                hello """),_display_(/*7.24*/{name}),format.raw/*7.30*/("""は

            </article>
            <aside>
                """),_display_(/*11.18*/commonSidebar()),format.raw/*11.33*/("""
            """),format.raw/*12.13*/("""</aside>
        </div>
    </section>
""")))}))
      }
    }
  }

  def render(name:String): play.twirl.api.HtmlFormat.Appendable = apply(name)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (name) => apply(name)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  DATE: 2019-07-21T20:30:28.365894
                  SOURCE: /home/a0131724/IdeaProjects/play-samples-play-scala-hello-world-tutorial/app/views/hello.scala.html
                  HASH: c5d1d63a67268e5a5444ea37d149050c528b70e3
                  MATRIX: 729->1|837->17|858->30|897->32|928->37|1093->176|1119->182|1209->245|1245->260|1286->273
                  LINES: 21->1|26->2|26->2|26->2|27->3|31->7|31->7|35->11|35->11|36->12
                  -- GENERATED --
              */
          